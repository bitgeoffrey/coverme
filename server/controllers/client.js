const Client = require('../models/client')
const Vehicle = require('../models/vehicle')
const { normalizeErrors } = require('../helpers/mongoose')

exports.createClient = function(req, res) {
    const { fname, lname, surname, idno, email, phone, addemails, addphones } = req.body;

    Client.findOne({ email }, (err, existingClient) => {
        if (err) {
            return res.status(422).send({ errors: normalizeErrors(err.errors) })
        }

        if (existingClient) {
            return res.status(422).send({ errors: [{ title: "Invalid Email", detail: "Client with the above email already exists" }] })
        }

        const client = new Client({
            fname,
            lname,
            surname,
            idno,
            email,
            phone,
            addemails,
            addphones
        })

        client.save((err) => {
            if (err) {
                return res.status(422).send({ errors: normalizeErrors(err.errors) })
            }
            return res.json({ 'Created client': true, client })
        });


    });
}

exports.allClients = function(req, res) {
    Client.find().where('deleted').equals(false)
        .populate('vehicles', 'regno -_id')
        .populate('policies', 'policyno inscompany policytype status -_id')
        .exec((err, foundClients) => {
            if (err) {
                return res.status(422).send({ errors: normalizeErrors(err.errors) })
            }

            return res.status(200).json(foundClients);
        });
}

exports.getClient = function(req, res) {
    const clientId = req.params.id
    Client.findById(clientId)
        .populate('vehicles', 'regno deleted -_id')
        .populate('policies', 'policyno inscompany policytype status -_id')
        .exec((err, foundClient) => {
            if (err) {
                return res.status(422).send({ errors: [{ title: "Client not found", detail: `Client with the requested id: ${clientId} was not found` }] });
            }

            return res.status(200).json(foundClient);
        });
}

exports.updateClient = function(req, res) {
    const clientId = req.params.id
        // const details = req.body
    const { fname, lname, surname, idno, phone, addemails, addphones } = req.body
        // Client.findByIdAndUpdate(clientId, details, { new: true }, (err, updatedClient) => {
        //     if (err) {
        //         return res.status(422).send({ errors: [{ title: "Error", detail: `Client with the id: ${clientId} was not updated` }] });
        //     }

    //     return res.status(200).json(updatedClient);
    // });

    Client.findByIdAndUpdate(clientId, { fname, lname, surname, idno, phone, addemails, addphones }, { new: true }, (err, updatedClient) => {
        if (err) {
            return res.status(422).send({ errors: [{ title: "Error", detail: `Client with the id: ${clientId} was not updated` }] });
        }

        const response = {
            message: "Client was successfully updated",
            updatedclient: updatedClient
        }

        return res.status(200).json(response);
    });

}

exports.removeClient = function(req, res) {
    const clientId = req.params.id

    Client.findByIdAndUpdate(clientId, { $set: { deleted: true } }, (err, removedClient) => {
        if (err) {
            return res.status(422).send({ errors: [{ title: "Error", detail: `Client with the id: ${clientId} was not deleted` }] });
        }
        const response = {
            message: "Client was successfully deleted",
            id: clientId
        }

        return res.status(200).json(response, removedClient);
    });

}