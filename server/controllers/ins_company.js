const InsCompany = require('../models/ins_company')
const { normalizeErrors } = require('../helpers/mongoose')

exports.createCompany = function (req, res) {
    const { name, location, address, email, phone } = req.body;

    InsCompany.findOne({ email }, (err, existingCompany) => {
        if (err) {
            return res.status(422).send({ errors: normalizeErrors(err.errors) })
        }

        if (existingCompany) {
            return res.status(422).send({ errors: [{ title: "Invalid Email", detail: `Insurance Company with the email: ${email} already exists` }] })
        }

        const company = new InsCompany({ name, location, address, email, phone })

        company.save((err) => {
            if (err) {
                return res.status(422).send({ errors: normalizeErrors(err.errors) })
            }
            return res.json({ 'Created Insurance Company': true, company })
        });


    });
}

exports.allCompanies = function (req, res) {
    InsCompany.find().where('deleted').equals(false)
    .populate('contactpersons', 'fname phone email -_id')
    .populate('policies', 'policyno policytype -_id')
    .exec((err, foundCompanies) => {
        if (err) {
            return res.status(422).send({ errors: normalizeErrors(err.errors) })
        }

        return res.status(200).json(foundCompanies);
    });
}

exports.getCompany = function (req, res) {
    const companyId = req.params.id
    InsCompany.findById(companyId)
    .populate('contactpersons', 'fname phone email -_id')
    .populate('policies', 'policyno policytype -_id')
    .exec((err, foundCompany) => {
        if (err) {
            return res.status(422).send({ errors: [{ title: "Insurance Company not found", detail: `Insurance Company with the id ${companyId} was not found` }] });
        }

        return res.status(200).json(foundCompany);
    });
}

exports.updateCompany = function (req, res) {
    const companyId = req.params.id
    const { name, location, address, phone } = req.body

    InsCompany.findByIdAndUpdate(companyId, { name, location, address, phone }, { new: true }, (err, updatedCompany) => {
        if (err) {
            return res.status(422).send({ errors: [{ title: "Error", detail: `Insurance Company with the id: ${companyId} was not updated` }] });
        }

        const response = {
            message: "Insurance Company was successfully updated",
            updatedcompany: updatedCompany
        }

        return res.status(200).json(response);
    });

}

exports.removeCompany = function (req, res) {
    const companyId = req.params.id

    InsCompany.findByIdAndUpdate(companyId, { $set: { deleted: true } }, (err, removedCompany) => {
        if (err) {
            return res.status(422).send({ errors: [{ title: "Error", detail: `Insurance Company with the id: ${companyId} was not deleted` }] });
        }
        const response = {
            message: "Insurance Company was successfully deleted",
            deletedcompany: removedCompany
        }

        return res.status(200).json(response);
    });

}
