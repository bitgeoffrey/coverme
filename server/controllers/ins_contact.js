const InsContact = require('../models/ins_contact')
const InsCompany = require('../models/ins_company')
const { normalizeErrors } = require('../helpers/mongoose')

exports.createContact = function (req, res) {
    const { fname, lname, surname, idno, email, phone, addemails, addphones, designation, inscompany } = req.body;

    InsContact.findOne({ email }, (err, existingContact) => {
        if (err) {
            return res.status(422).send({ errors: normalizeErrors(err.errors) })
        }

        if (existingContact) {
            return res.status(422).send({ errors: [{ title: "Invalid Email", detail: `Contact Person with the email: ${email} already exists` }] })
        }

        const contactperson = new InsContact({ fname, lname, surname, idno, email, phone, addemails, addphones, designation, inscompany })

        contactperson.save((err) => {
            if (err) {
                return res.status(422).send({ errors: normalizeErrors(err.errors) })
            }
            InsCompany.update({_id: contactperson.inscompany}, {$push: {contactpersons: contactperson}}, (err)=>{
                if (err) {
                    return res.status(422).send({ errors: normalizeErrors(err.errors) })
                }
            });

            return res.json({ 'Created Contact Person': true, contactperson })
        });


    });
}

exports.allContacts = function (req, res) {
    InsContact.find().where('deleted').equals(false).populate('inscompany', 'name location -_id').exec((err, foundContacts) => {
        if (err) {
            return res.status(422).send({ errors: normalizeErrors(err.errors) })
        }

        return res.status(200).json(foundContacts);
    });
}

exports.getContact = function (req, res) {
    const contactId = req.params.id
    InsContact.findById(contactId).populate('inscompany', 'name location -_id').exec((err, foundContact) => {
        if (err) {
            return res.status(422).send({ errors: [{ title: "Contact Person not found", detail: `Contact Person with the id ${contactId} was not found` }] });
        }

        return res.status(200).json(foundContact);
    });
}

exports.updateContact = function (req, res) {
    const contactId = req.params.id
    const { fname, lname, surname, idno, phone, addemails, addphones, designations } = req.body

    InsContact.findByIdAndUpdate(contactId, { fname, lname, surname, idno, phone, addemails, addphones, designations }, { new: true }, (err, updatedContact) => {
        if (err) {
            return res.status(422).send({ errors: [{ title: "Error", detail: `Contact Person Company with the id: ${contactId} was not updated` }] });
        }
        const response = {
            message:"Contact person was successfully updated",
            updatedcontact: updatedContact
        }

        return res.status(200).json(response);
    });

}

exports.removeContact = function (req, res) {
    const contactId = req.params.id

    InsContact.findByIdAndUpdate(contactId, { $set: { deleted: true } }, (err, removedContact) => {
        if (err) {
            return res.status(422).send({ errors: [{ title: "Error", detail: `Contact Person with the id: ${contactId} was not deleted` }] });
        }
        const response = {
            message: "Contact Person was successfully deleted",
            company: removedContact
        }

        return res.status(200).json(response);
    });

}

