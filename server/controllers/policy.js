const Policy = require('../models/policy')
const Client = require('../models/client')
const InsCompany = require('../models/ins_company')
const Policytype = require('../models/policy_type')

const { normalizeErrors } = require('../helpers/mongoose')

exports.createPolicy = function(req, res) {
    const { policyno, inscompany, client, policytype, policybenefits, suminsured, commence, end, status, remarks, tags, risknote, docs } = req.body;

    Policy.findOne({ policyno }, (err, existingPolicy) => {
        if (err) {
            return res.status(422).send({ errors: normalizeErrors(err.errors) })
        }

        if (existingPolicy) {
            return res.status(422).send({ errors: [{ title: "Invalid Policy Number", detail: `Policy number ${policyno} already exists` }] })
        }

        const policy = new Policy({ policyno, inscompany, client, policytype, policybenefits, suminsured, commence, end, status, remarks, tags, risknote, docs })

        policy.save((err, newPolicy) => {
            if (err) {
                return res.status(422).send({ errors: normalizeErrors(err.errors) })
            }

            Client.update({ _id: policy.client }, { $push: { policies: newPolicy } }, (err) => {
                if (err) {
                    return res.status(422).send({ errors: normalizeErrors(err.errors) })
                }
            });

            InsCompany.update({ _id: policy.inscompany }, { $push: { policies: newPolicy } }, (err) => {
                if (err) {
                    return res.status(422).send({ errors: normalizeErrors(err.errors) })
                }
            });

            Policytype.update({ _id: policy.policytype }, { $push: { policies: newPolicy } }, (err) => {
                if (err) {
                    return res.status(422).send({ errors: normalizeErrors(err.errors) })
                }
            });


            return res.json({ 'Created new Policy': true, newPolicy })
        });


    });
}

exports.allPolicies = function(req, res) {
    Policy.find().where('deleted').equals(false)
        .populate('inscompany', 'name phone email -_id')
        .populate('client', 'fname lname surname email phone -_id')
        .populate('policytype', 'name -_id')
        .populate('policybenefits', 'name -_id')
        // .populate('risknote', '-_id')
        // .populate('docs', '-_id')
        .exec((err, foundPolicies) => {
            if (err) {
                return res.status(422).send({ errors: normalizeErrors(err.errors) })
            }

            return res.status(200).json(foundPolicies);
        });
}

exports.getPolicy = function(req, res) {
    const policyId = req.params.id
    Policy.findById(policyId)
        .populate('inscompany', 'name phone email -_id')
        .populate('client', 'fname lname surname email phone -_id')
        .populate('policytype', 'name -_id')
        .populate('policybenefits', 'name -_id')
        // .populate('risknote', '-_id')
        // .populate('docs', '-_id')
        .exec((err, foundPolicy) => {
            if (err) {
                return res.status(422).send({ errors: [{ title: "Policy not found", detail: `Policy with the id: ${policyId} was not found` }] });
            }

            return res.status(200).json(foundPolicy);
        });
}

exports.getClientPolicy = function(req, res) {
    const clientId = req.params.id
    Policy.find()
        .where('deleted').equals(false)
        .where('client').equals(clientId)
        .populate('inscompany', 'name phone email -_id')
        .populate('client', 'fname lname surname email phone -_id')
        .populate('policytype', 'name -_id')
        .populate('policybenefits', 'name -_id')
        // .populate('risknote', '-_id')
        // .populate('docs', '-_id')
        .exec((err, foundClientPolicy) => {
            if (err) {
                return res.status(422).send({ errors: [{ title: "Policy not found", detail: `Policy with the client: ${clientId} was not found` }] });
            }

            return res.status(200).json(foundClientPolicy);
        });
}

exports.updatePolicy = function(req, res) {
    const policyId = req.params.id
    const { inscompany, client, policytype, policybenefits, suminsured, commence, end, status, remarks, tags, risknote, docs } = req.body


    Policy.findByIdAndUpdate(policyId, { inscompany, client, policytype, policybenefits, suminsured, commence, end, status, remarks, tags, risknote, docs }, { new: true }, (err, updatedPolicy) => {
        if (err) {
            return res.status(422).send({ errors: [{ title: "Error", detail: `Policy with the id: ${policyId} was not updated` }] });
        }

        const response = {
            message: "Policy was successfully updated",
            updatedclient: updatedPolicy
        }

        return res.status(200).json(response);
    });

}

exports.removePolicy = function(req, res) {
    const policyId = req.params.id

    Policy.findByIdAndUpdate(policyId, { $set: { deleted: true } }, (err, removedPolicy) => {
        if (err) {
            return res.status(422).send({ errors: [{ title: "Error", detail: `Policy with the id: ${policyId} was not deleted` }] });
        }
        const response = {
            message: "Policy was successfully deleted",
            removedPolicy: removedPolicy
        }

        return res.status(200).json(response);
    });

}