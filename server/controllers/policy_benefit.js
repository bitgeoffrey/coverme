const Policybenefit = require('../models/policy_benefit');
const normalizeErrors = require('../helpers/mongoose');

exports.allPolicybenefits = function (req, res) {
    Policybenefit.find().where('deleted').equals(false).exec((err, foundPolicybenefits) => {
        if (err) {
            return res.status(422).send({ errors: normalizeErrors(err.errors) })
        }

        return res.status(200).json(foundPolicybenefits);
    });
}

exports.getPolicybenefit = function (req, res) {
    const policybenefitId = req.params.id
    Policybenefit.findById(policybenefitId).exec((err, foundPolicybenefit) => {
        if (err) {
            return res.status(422).send({ errors: [{ title: "Policy benefit not found", detail: `Policy Benefit the id ${policybenefitId} was not found` }] });
        }

        return res.status(200).json(foundPolicybenefit);
    });
}

exports.createPolicybenefit = function (req, res) {
    const { name } = req.body;

    Policybenefit.findOne({ name }, (err, existingPolicybenefit) => {
        if (err) {
            return res.status(422).send({ errors: normalizeErrors(err.errors) })
        }

        if (existingPolicybenefit) {
            return res.status(422).send({ errors: [{ title: "Confirm Policy Benefit", detail: `Policy Benefit: ${name} already exists` }] })
        } else {
            const policybenefit = new Policybenefit({ name});

            Policybenefit.create(policybenefit, (err, newPolicyBenefit) => {
                if (err) {
                    return res.status(422).send({ errors: normalizeErrors(err.errors) })
                }

                return res.json({ 'Policy Benefit was added': true, newPolicyBenefit })
            });

        }
    });
}

exports.updatePolicybenefit = function (req, res) {
    const policybenefitId = req.params.id
    const { name } = req.body // explicit fields

    Policybenefit.findByIdAndUpdate(policybenefitId, { name }, { new: true }, (err, updatedPolicybenefit) => {
        if (err) {
            return res.status(422).send({ errors: [{ title: "Error", detail: `Policy Benefit with the id: ${policybenefitId} failed to update` }] });
        }

        const response = {
            message: "Policy Benefit was successfully updated",
            vehicle: updatedPolicybenefit
        }

        return res.status(200).json(response);

    });

}

exports.removePolicybenefit = function (req, res) {
    const policybenefitId = req.params.id

    Policybenefit.findByIdAndUpdate(policybenefitId, { $set: { deleted: true } }, (err, removedPolicybenefit) => {
        if (err) {
            return res.status(422).send({ errors: [{ title: "Error", detail: `Policy type with the id: ${policybenefitId} was not deleted` }] });
        }
        const response = {
            message: "Policy Benefit was successfully deleted",
            vehicle: removedPolicybenefit
        }

        return res.status(200).json(response);
    });

}