const Policytype = require('../models/policy_type');
const normalizeErrors = require('../helpers/mongoose');

exports.allPolicytypes = function (req, res) {
    Policytype.find().where('deleted').equals(false)
    .populate('policies', 'policyno inscompany -_id')
    .exec((err, foundPolicytypes) => {
        if (err) {
            return res.status(422).send({ errors: normalizeErrors(err.errors) })
        }

        return res.status(200).json(foundPolicytypes);
    });
}

exports.getPolicytype = function (req, res) {
    const policytypeId = req.params.id
    Policytype.findById(policytypeId)
    .populate('policies', 'policyno inscompany -_id')
    .exec((err, foundPolicytype) => {
        if (err) {
            return res.status(422).send({ errors: [{ title: "Policy type not found", detail: `Policy Type the id ${policytypeId} was not found` }] });
        }

        return res.status(200).json(foundPolicytype);
    });
}

exports.createPolicytype = function (req, res) {
    const { name, code } = req.body;

    Policytype.findOne({ code }, (err, existingPolicytype) => {
        if (err) {
            return res.status(422).send({ errors: normalizeErrors(err.errors) })
        }

        if (existingPolicytype) {
            return res.status(422).send({ errors: [{ title: "Confirm Policy Name and Code", detail: `Policy Type with name: ${name} and code: ${code} already exists` }] })
        } else {
            const policytype = new Policytype({ name, code });

            Policytype.create(policytype, (err, newPolicyType) => {
                if (err) {
                    return res.status(422).send({ errors: normalizeErrors(err.errors) })
                }

                return res.json({ 'Policy Type was added': true, newPolicyType })
            });

        }
        // vehicle.save((err) => {
        //     if (err) {
        //         return res.status(422).send({ errors: normalizeErrors(err.errors) })
        //     }
        //     return res.json({ 'Client Vehicle was added': true, vehicle })
        // });


    });
}

exports.updatePolicytype = function (req, res) {
    const policytypeId = req.params.id
    const { name } = req.body // explicit fields

    Policytype.findByIdAndUpdate(policytypeId, { name }, { new: true }, (err, updatedPolicytype) => {
        if (err) {
            return res.status(422).send({ errors: [{ title: "Error", detail: `Policy Type with the id: ${policytypeId} failed to update` }] });
        }

        const response = {
            message: "Policy type was successfully updated",
            vehicle: updatedPolicytype
        }

        return res.status(200).json(response);

    });

}

exports.removePolicytype = function (req, res) {
    const policytypeId = req.params.id

    Policytype.findByIdAndUpdate(policytypeId, { $set: { deleted: true } }, (err, removedPolicytype) => {
        if (err) {
            return res.status(422).send({ errors: [{ title: "Error", detail: `Policy type with the id: ${policytypeId} was not deleted` }] });
        }
        const response = {
            message: "Policy Type was successfully deleted",
            vehicle: removedPolicytype
        }

        return res.status(200).json(response);
    });

}