const Vehicle = require('../models/vehicle')
const Client = require('../models/client')
const { normalizeErrors } = require('../helpers/mongoose')



exports.allVehicles = function(req, res) {
    Vehicle.find().where('deleted').equals(false).populate('client', 'fname lname email phone -_id').exec((err, foundVehicles) => {
        if (err) {
            return res.status(422).send({ errors: normalizeErrors(err.errors) })
        }

        return res.status(200).json(foundVehicles);
    });
}

exports.getVehicle = function(req, res) {
    const vehicleId = req.params.id
    Vehicle.findById(vehicleId).populate('client', 'fname lname email phone -_id').exec((err, foundVehicle) => {
        if (err) {
            return res.status(422).send({ errors: [{ title: "Vehicle not found", detail: `Vehicle with the id ${vehicleId} was not found` }] });
        }

        return res.status(200).json(foundVehicle);
    });
}

exports.getClientVehicles = function(req, res) {
    const clientId = req.params.id
    Vehicle.find().where('deleted').equals(false).where('client').equals(clientId).exec((err, clientVehicles) => {
        if (err) {
            return res.status(422).send({ errors: [{ title: "An Error occurred", detail: `Vehicles for client: ${clientId} could not be fetched` }] });
        }

        console.log(clientVehicles)

        return res.status(200).json(clientVehicles);
    });
}

exports.createVehicle = function(req, res) {
    const { regno, make, model, yom, value, client } = req.body;

    Vehicle.findOne({ regno }, (err, existingVehicle) => {
        if (err) {
            return res.status(422).send({ errors: normalizeErrors(err.errors) })
        }

        if (existingVehicle) {
            return res.status(422).send({ errors: [{ title: "Confirm Vehicle Registration", detail: `Vehicle with the Registration No: ${regno} already exists` }] })
        } else {
            const vehicle = new Vehicle({ regno, make, model, yom, value, client, });

            Vehicle.create(vehicle, (err, newVehicle) => {
                if (err) {
                    return res.status(422).send({ errors: normalizeErrors(err.errors) })
                }
                Client.update({ _id: vehicle.client }, { $push: { vehicles: newVehicle } }, (err, clientUpdated) => {
                    if (err) {
                        return res.status(422).send({ errors: normalizeErrors(err.errors) })
                    }
                    // res.json({ 'Client was updated with the new vehicle': true, clientUpdated })

                });

                return res.json({ 'Client Vehicle was added': true, newVehicle })
            });

        }
        // vehicle.save((err) => {
        //     if (err) {
        //         return res.status(422).send({ errors: normalizeErrors(err.errors) })
        //     }
        //     return res.json({ 'Client Vehicle was added': true, vehicle })
        // });


    });
}

exports.updateVehicle = function(req, res) {
    const vehicleId = req.params.id
    const { make, model, yom, value } = req.body // explicit fields

    Vehicle.findByIdAndUpdate(vehicleId, { make, model, yom, value }, { new: true }, (err, updatedVehicle) => {
        if (err) {
            return res.status(422).send({ errors: [{ title: "Error", detail: `Vehicle with the id: ${vehicleId} failed to update` }] });
        }

        const response = {
            message: "Vehicle was successfully updated",
            vehicle: updatedVehicle
        }

        return res.status(200).json(response);

    });

}

exports.removeVehicle = function(req, res) {
    const vehicleId = req.params.id

    Vehicle.findByIdAndUpdate(vehicleId, { $set: { deleted: true } }, (err, removedVehicle) => {
        if (err) {
            return res.status(422).send({ errors: [{ title: "Error", detail: `Vehicle with the id: ${vehicleId} was not deleted` }] });
        }
        const response = {
            message: "Vehicle was successfully deleted",
            vehicle: removedVehicle
        }

        return res.status(200).json(response);
    });

}