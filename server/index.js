const express = require('express'),
      mongoose = require('mongoose'),
      bodyParser = require('body-parser'),
      cors = require('cors');
      clientRouter = require('./routes/clients');
      vehicleRouter = require('./routes/vehicles');
      policytypeRouter = require('./routes/policy_types');
      policybenefitRouter = require('./routes/policy_benefits');
      inscompanyRouter = require('./routes/ins_company');
      inscontactRouter = require('./routes/ins_contacts');
      policyRouter = require('./routes/policies');

const config = require('./config/DB')


mongoose.connect(config.DB_URI, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false })
.then(()=>{console.log('success database connected')})
.catch(()=>{console.log('Sorry an error occurred while connecting to the database')});

const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

const corsOptions = {
    origin: 'http://localhost:4201',
    optionsSuccessStatus: 200
}

app.use(cors(corsOptions));

app.use('/api/clients', clientRouter);
app.use('/api/vehicles', vehicleRouter);
app.use('/api/policytypes', policytypeRouter);
app.use('/api/policybenefits', policybenefitRouter);
app.use('/api/inscompany', inscompanyRouter);
app.use('/api/inscontact', inscontactRouter);
app.use('/api/policies', policyRouter);



const PORT = process.env.PORT || 3002

app.listen(PORT, ()=>{
    console.log(`server running on port: ${PORT}`)
});