const mongoose = require('mongoose');

const Schema = mongoose.Schema

const clientSchema = new Schema({
    fname: { type: String, required: 'First Name is required' },
    lname: { type: String, required: 'Last Name is required' },
    surname: { type: String, required: false },
    idno: { type: Number, required: 'ID Number is required' },
    email: { type: String, required: 'Primary email is required', unique: true, match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/] },
    phone: { type: String, required: false },
    createdAt: { type: Date, default: Date.now },
    addemails: [{ type: String, default: "app@coverme.com" }],  //remove default
    addphones: [{ type: Number, required: false }],
    vehicles: [{type: Schema.Types.ObjectId, ref: 'Vehicle'}],
    policies: [{type: Schema.Types.ObjectId, ref:'Policy'}],
    deleted: { type: Boolean, default: 0 }
});

module.exports = mongoose.model('Client', clientSchema)