const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const InsCompanySchema = new Schema({
    name: { type: String, required: "Insurance Company Name is required", unique: true },
    location: { type: String, required: false },
    address: { type: String, required: false },
    phone: { type: Number, required: "Phone Number is required" },
    email: { type: String, required: "Email is required", unique: true, match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/]},
    contactpersons: [{type: Schema.Types.ObjectId, ref: 'InsContact'}],
    policies: [{type: Schema.Types.ObjectId, ref: 'Policy'}],
    createdAt: { type: Date, default: Date.now },
    deleted: { type: Boolean, default: 0 }
});

module.exports = mongoose.model('InsCompany', InsCompanySchema)