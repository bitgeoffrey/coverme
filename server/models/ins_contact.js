const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const InsContactSchema = new Schema({
    fname: { type: String, required: 'First Name is required' },
    lname: { type: String, required: false },
    surname: { type: String, required: false },
    idno: { type: Number, required: false },
    email: { type: String, required: 'Primary email is required', unique: true, match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/] },
    phone: { type: Number, required: false },
    addemails: [{ type: String, required: false }],
    addphones: [{ type: Number, required: false }],
    inscompany: [{ type: Schema.Types.ObjectId, ref: 'InsCompany' }],
    designation: { type: String, required: false },
    createdAt: { type: Date, default: Date.now },
    deleted: { type: Boolean, default: 0 }
});

module.exports = mongoose.model('InsContact', InsContactSchema)