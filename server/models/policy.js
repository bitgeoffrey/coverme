const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PolicySchema = new Schema({
    policyno: { type: String, required: "Policy Number is required", unique: true },
    inscompany: { type: Schema.Types.ObjectId, ref:'InsCompany', required: "Insurance Company is required" },
    client: { type: Schema.Types.ObjectId, ref:'Client', required: "Client is required" },
    policytype: { type: Schema.Types.ObjectId, ref:'Policytype', required: "Policy Type is required" },
    policybenefits:[ { type: Schema.Types.ObjectId, ref:'Policybenefit', required: false }], //consider switching to required
    suminsured: {type:String, required: "Sum Insured is required"},
    commence: {type: Date, required: "Policy commencement date is required"},
    end: {type: Date, required: "Policy end date is required"},
    status: {type: String, default: "active"},
    remarks: {type: String, required: false},
    tags:[{type: String, required: false}],
    // risknote:{ type: Schema.Types.ObjectId, ref:'Risknote', required: false },
    // docs:[ { type: Schema.Types.ObjectId, ref:'PolicyDoc', required: false }],
    risknote:{  },
    docs:[ {  }],
    createdAt: { type: Date, default: Date.now },
    deleted: { type: Boolean, default: 0 }

});

module.exports = mongoose.model('Policy', PolicySchema)