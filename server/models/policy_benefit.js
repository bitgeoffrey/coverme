const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PolicybenefitSchema = new Schema({
    name: { type: String, required: "Policy Benefit Name is required", unique: true },
    createdAt: { type: Date, default: Date.now },
    deleted: { type: Boolean, default: 0 }
});

module.exports = mongoose.model('Policybenefit', PolicybenefitSchema);

