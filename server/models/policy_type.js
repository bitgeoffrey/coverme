const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PolicytypeSchema = new Schema({
    name: { type: String, required: true, unique: true },
    code: { type: String, required: true, unique: true },  // switch required to true
    policies: [{type: Schema.Types.ObjectId, ref:'Policy'}],
    createdAt: { type: Date, default: Date.now },
    deleted: { type: Boolean, default: 0 }
});

module.exports = mongoose.model('Policytype', PolicytypeSchema)

