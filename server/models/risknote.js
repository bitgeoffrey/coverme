const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const RisknoteSchema = new Schema({
    policy: {type: Schema.Types.ObjectId, ref:'Policy'},
    risknote: { },
    createdAt: { type: Date, default: Date.now },
    deleted: { type: Boolean, default: 0 }
});

module.exports = mongoose.model('Risknote', RisknoteSchema);

