const mongoose = require('mongoose');
const Schema = mongoose.Schema


const vehicleSchema = new Schema({
    regno: { type: String, required: true, unique: true },
    make: { type: String, required: true },
    model: { type: String, required: true },
    yom: { type: Number, required: true },
    value: { type: String, required: false },
    createdAt: { type: Date, default: Date.now },
    deleted: { type: Boolean, default: 0 },
    client: { type: Schema.Types.ObjectId, ref: 'Client', required: false } // switch to required true
});


module.exports = mongoose.model('Vehicle', vehicleSchema);