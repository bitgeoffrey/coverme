const express = require('express')
const router = express.Router()
const clientCtrl = require('../controllers/client')

//client personal details
router.post('', clientCtrl.createClient);

router.get('/', clientCtrl.allClients);

router.get('/:id', clientCtrl.getClient);

router.post('/:id', clientCtrl.updateClient);

router.post('/remove/:id', clientCtrl.removeClient)



//client vehicle details



module.exports = router
