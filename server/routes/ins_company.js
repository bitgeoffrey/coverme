const express = require('express')
const router = express.Router()
const companyCtrl = require('../controllers/ins_company')

//client personal details
router.post('', companyCtrl.createCompany);

router.get('/', companyCtrl.allCompanies);

router.get('/:id', companyCtrl.getCompany);

router.post('/:id', companyCtrl.updateCompany);

router.post('/remove/:id', companyCtrl.removeCompany)



//client vehicle details



module.exports = router
