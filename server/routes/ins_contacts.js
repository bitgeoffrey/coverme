const express = require('express')
const router = express.Router()
const contactCtrl = require('../controllers/ins_contact')

//client personal details
router.post('', contactCtrl.createContact);

router.get('/', contactCtrl.allContacts);

router.get('/:id', contactCtrl.getContact);

router.post('/:id', contactCtrl.updateContact);

router.post('/remove/:id', contactCtrl.removeContact)



//client vehicle details



module.exports = router
