const express = require('express')
const router = express.Router()
const policyCtrl = require('../controllers/policy')

//client personal details
router.post('', policyCtrl.createPolicy);

router.get('/', policyCtrl.allPolicies);

router.get('/:id', policyCtrl.getPolicy);

router.get('/client/:id', policyCtrl.getClientPolicy);

router.post('/:id', policyCtrl.updatePolicy);

router.post('/remove/:id', policyCtrl.removePolicy)



//client vehicle details



module.exports = router