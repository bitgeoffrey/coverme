const express = require('express');
const router = express.Router();
const policybenefitCtrl = require('../controllers/policy_benefit');

router.get('/', policybenefitCtrl.allPolicybenefits);

router.get('/:id', policybenefitCtrl.getPolicybenefit);

router.post('', policybenefitCtrl.createPolicybenefit);

router.post('/:id', policybenefitCtrl.updatePolicybenefit);

router.post('/remove/:id', policybenefitCtrl.removePolicybenefit)


module.exports = router;
