const express = require('express');
const router = express.Router();
const policytypeCtrl = require('../controllers/policy_type');

router.get('/', policytypeCtrl.allPolicytypes);

router.get('/:id', policytypeCtrl.getPolicytype);

router.post('', policytypeCtrl.createPolicytype);

router.post('/:id', policytypeCtrl.updatePolicytype);

router.post('/remove/:id', policytypeCtrl.removePolicytype)


module.exports = router;
