const express = require('express');
const router = express.Router();
const vehicleCtrl = require('../controllers/vehicle');

router.get('/', vehicleCtrl.allVehicles);

router.get('/:id', vehicleCtrl.getVehicle);

router.get('/client/:id', vehicleCtrl.getClientVehicles);

router.post('', vehicleCtrl.createVehicle);

router.post('/:id', vehicleCtrl.updateVehicle);

router.post('/remove/:id', vehicleCtrl.removeVehicle)


module.exports = router;