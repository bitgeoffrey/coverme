import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatSnackBar} from '@angular/material/snack-bar';


import { DataSource } from '@angular/cdk/collections';

import { BehaviorSubject, fromEvent, merge, Observable } from 'rxjs';
import { map, debounceTime, distinctUntilChanged } from 'rxjs/operators';

import {MatDialog, MAT_DIALOG_DEFAULT_OPTIONS} from '@angular/material/dialog';
import { FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';

import { HttpErrorResponse, HttpClient } from '@angular/common/http';

import { Client } from '../client/common/client.model';
import { ClientService } from '../client/common/client.service'
import { Router } from '@angular/router';


@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css'],
  providers: [
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: false}}
  ]
})
export class ClientComponent implements OnInit {
  displayedColumns = ['fname', 'lname', 'email', 'phone', 'actions'];

  clientData: ClientService | null;
  dataSource: ClientDataSource | null;
  index: number;
  id: string;
  client: Client

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;

  constructor( public dialog: MatDialog,public httpClient: HttpClient, private router: Router, private clientService: ClientService, private _snackBar: MatSnackBar) {  }

  ngOnInit() {
    this.loadData();
  }

  openAddClient() {
    const dialogRef = this.dialog.open(AddClient);

  }

  refresh() {
    this.loadData();
  }

  // showClient(clientId) {
  //   this.clientService.getClient(clientId).subscribe(
  //     (data: Client)=>{
  //       this.client = data;
  //       console.log(this.client)
  //       this.router.navigate([`/admin/clients/${clientId}`]);
  //     },
  //     (errorResponse: HttpErrorResponse)=>{
  //       this._snackBar.open(errorResponse.error.errors[0].detail, '', { duration: 4000 , panelClass : 'error_snack' })
  //     }
  //   )
  // }

  showClient(clientId) {
        console.log(clientId)
        this.router.navigate([`/admin/clients/${clientId}`]);
  }

  // addVehicle(issue: Issue) {
  //   const dialogRef = this.dialog.open(AddDialogComponent, {
  //     data: { issue: issue }
  //   });

  //   dialogRef.afterClosed().subscribe(result => {
  //     if (result === 1) {
  //       // After dialog is closed we're doing frontend updates
  //       // For add we're just pushing a new row inside DataService
  //       this.vehicleData.dataChange.value.push(this.dataService.getDialogData());
  //       this.refreshTable();
  //     }
  //   });
  // }


  // editVehicle(i: number, _id: string, regno: string, make: string, model: string, yom: string, value: string, createdAt: string) {
  //   // this.id = _id;
  //   // index row is used just for debugging proposes and can be removed
  //   this.index = i;
  //   // console.log(this.index);
  //   // console.log(_id)
  //   const dialogRef = this.dialog.open(EditDialogComponent, {
  //     data: { id: _id, regno: regno, make: make, model: model, yom: yom, value: value},
      
  //   });


  //   dialogRef.afterClosed().subscribe(result => {
  //     if (result === 1) {
  //       // When using an edit things are little different, firstly we find record inside DataService by id
  //       const foundIndex = this.vehicleData.dataChange.value.findIndex(x => x._id === this.id);
  //       console.log(foundIndex)
  //       // Then you update that record using data from dialogData (values you enetered)
  //       this.vehicleData.dataChange.value[foundIndex] = this.dataService.getDialogData();
  //       // And lastly refresh table
  //       this.refreshTable();
  //     }
  //   });
  // }

  // deleteVehicle(i: number, _id: string, regno: string, make: string, model: string, yom: string, value: string) {
  //   this.index = i;
  //   // this.id = _id;
  //   const dialogRef = this.dialog.open(DeleteDialogComponent, {
  //     data: { id: _id, regno: regno, make: make, model: model, yom: yom, value: value }
  //   });

  //   dialogRef.afterClosed().subscribe(result => {
  //     if (result === 1) {
  //       const foundIndex = this.vehicleData.dataChange.value.findIndex(x => x._id === this.id);
  //       // for delete we use splice in order to remove single object from DataService
  //       this.vehicleData.dataChange.value.splice(foundIndex, 1);
  //       this.refreshTable();
  //     }
  //   });
  // }

  public refreshTable() {
    this.paginator._changePageSize(this.paginator.pageSize);
  }

  public loadData() {
    this.clientData = new ClientService(this.httpClient);
    this.dataSource = new ClientDataSource(this.clientData, this.paginator, this.sort);
    fromEvent(this.filter.nativeElement, 'keyup')
      .pipe(debounceTime(150), distinctUntilChanged())
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }


}

@Component({
  selector: 'add-client',
  templateUrl: 'add-client.html',
  styleUrls:['./add-client.css']
})
export class AddClient implements OnInit{
  addClientFG: FormGroup

  newClient: Client
  errors: HttpErrorResponse[] = []
  moreEmails: Boolean = false;
  morePhones: Boolean = false;


  form_validation_messages = {
    'fname': [
      { type: 'required', message: 'First Name is required' },
    ],
    'lname': [
      { type: 'required', message: 'Last Name is required' },
    ],
    'idno': [
      { type: 'required', message: 'ID / Passport Number is required' },
    ],
    'email': [
      { type: 'required', message: 'Email is required' },
      { type: 'pattern', message: 'Enter a valid email' }
    ],
    'phone': [
      { type: 'required', message: 'Phone number is required' },
      { type: 'pattern', message: 'Enter a valid phone number' }
    ],
    /*
    // 'confirm_password': [
    //   { type: 'required', message: 'Confirm password is required' },
    //   { type: 'areEqual', message: 'Password mismatch' }
    // ],
    // 'password': [
    //   { type: 'required', message: 'Password is required' },
    //   { type: 'minlength', message: 'Password must be at least 5 characters long' },
    //   { type: 'pattern', message: 'Your password must contain at least one uppercase, one lowercase, and one number' }
    // ],
    // 'terms': [
    //   { type: 'pattern', message: 'You must accept terms and conditions' }
    // ] 
    */
    }


  // mobnumPattern = '^((\\+?)|0)?[0-9]{0,20}$';
  // emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';

  constructor(private fb: FormBuilder, private clientService: ClientService, public _snackBar: MatSnackBar) {}

  ngOnInit() {
    this.addClientFG = this.fb.group({
      fname: ['', Validators.required],
      lname: ['', Validators.required],
      surname: [''],
      idno: ['', Validators.required],
      email: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$')]],
      phone: ['', [ Validators.required, Validators.pattern('^[0-9]{10}$')] ],
      addemails: this.fb.array([ ]),
      addphones: this.fb.array([ ])
    });

  }

  get addemails() {
    return this.addClientFG.get('addemails') as FormArray;
  }

  addEmail() {
    this.moreEmails =true;
    this.addemails.push(this.fb.control('', [Validators.pattern('^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$')]));
  }

  removeEmail(index: number) {
    if (this.addemails.length !== 0) {
        this.addemails.removeAt(index);
    }
  }

  get addphones() {
    return this.addClientFG.get('addphones') as FormArray;
  }

  addPhone() {
    this.morePhones =true;
    this.addphones.push(this.fb.control('',  [Validators.pattern('^[0-9]{10}$')]));
  }

  removePhone(index: number) {
    if (this.addphones.length !== 0) {
        this.addphones.removeAt(index);
    } 
  }

  addClient() {
    this.clientService.addClient(this.addClientFG.value).subscribe(
      ()=>{
        this._snackBar.open("Success Client was Added", '', { duration: 4000, panelClass : 'success_snack' })
      },
      (errorResponse: HttpErrorResponse)=>{
        this._snackBar.open(errorResponse.error.errors[0].detail, '', { duration: 4000 , panelClass : 'error_snack' })
      }
    );
    
  }


}

//Client Data source

export class  ClientDataSource extends DataSource<Client> {
  _filterChange = new BehaviorSubject('');

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: Client[] = [];

  renderedData: Client[] = [];


  constructor(public _clientService: ClientService,
    public _paginator: MatPaginator,
    public _sort: MatSort) {
    super();
    // Reset to the first page when the user changes the filter.
    this._filterChange.subscribe(() => this._paginator.pageIndex = 0);
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<Client[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this._clientService.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];
    this._clientService.getAllClients();


    return merge(...displayDataChanges).pipe(map(() => {
      this.filteredData = this._clientService.data.slice().filter((client: Client) => {
        const searchStr = (client.fname + client.lname + client.email + client.phone).toLowerCase();
        return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
      });

      // Sort filtered data
      const sortedData = this.sortData(this.filteredData.slice());

      // Grab the page's slice of the filtered sorted data.
      const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
      this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
      return this.renderedData;
    }
    ));
  }

  disconnect() { }


  /** Returns a sorted copy of the database data. */
    sortData(data: Client[]): Client[] {

    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';

      switch (this._sort.active) {
        case 'fname': [propertyA, propertyB] = [a.fname, b.fname]; break;
        case 'lname': [propertyA, propertyB] = [a.lname, b.lname]; break;
        case 'email': [propertyA, propertyB] = [a.email, b.email]; break;
        case 'phone': [propertyA, propertyB] = [a.phone, b.phone]; break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    });
  }
}

