import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { AppMaterialModule } from '../../app-material.module';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AdminComponent } from '../admin.component';
import { ClientComponent, AddClient } from '../client/client.component';
import { DeleteClientComponent } from './dialogs/delete/deleteclient.dialog.component';
import { EditClientComponent } from './dialogs/edit/editclient.dialog.component';
import { ShowClientComponent } from './show/showclient.component';

const routes: Routes = [
  {
    path: 'admin', component: AdminComponent, children: [
      { path: 'clients', component: ClientComponent },
      { path:'clients/:id', component: ShowClientComponent}
    ]
  }
];

@NgModule({
  declarations: [
    ClientComponent,
    AddClient,
    DeleteClientComponent,
    EditClientComponent,
    ShowClientComponent
  ],
  entryComponents: [
    AddClient,
    DeleteClientComponent,
    EditClientComponent,
    ShowClientComponent
  ],
  imports: [
    RouterModule.forRoot(routes),
    CommonModule,
    AppMaterialModule,
    ReactiveFormsModule,
    FlexLayoutModule
  ],
  providers: [],
})
export class ClientModule { }
