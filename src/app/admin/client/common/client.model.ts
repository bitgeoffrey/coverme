export class Client {
    _id?: string;
    fname?: string;
    lname?: string;
    surname?: string;
    idno?: number;
    email?: string;
    phone?: number;
    addemails?: [];
    addphones?: [];
    deleted: Boolean
}
