import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

import { Client } from '../common/client.model'
import { Vehicle } from '../../vehicle/common/vehicle.model';


@Injectable({
  providedIn: 'root'
})
export class ClientService {
  url = 'http://localhost:3001/api/clients'
  urlclientvehicles = 'http://localhost:3001/api/vehicles'
  urlclientpolicies = 'http://localhost:3001/api/policies'

  dataChange: BehaviorSubject<Client[]> = new BehaviorSubject<Client[]>([]);
  dataChangeVehicle: BehaviorSubject<Vehicle[]> = new BehaviorSubject<Vehicle[]>([]);



  constructor(private httpClient: HttpClient) { }

  // private handleError(error: HttpErrorResponse) {
  //   if(error.error instanceof ErrorEvent) {
  //     console.error('A error occurred'  + error.error.message)
  //   }
  //   else {
  //     console.error(`Backend error: ${error.status} body: ${error.error}` )
  //   }

  //   return throwError( 'Sorry an error occurred, please try again later')
  // }

  get data(): Client[] {
    return this.dataChange.value;
  }

  get dataclientvehicle(): Vehicle[] {
    return this.dataChangeVehicle.value;
  }

  getAllClients(): void {
    this.httpClient.get<Client[]>(this.url).subscribe(data => {

      this.dataChange.next(data);
    },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      });
  }

  addClient(client: Client) {
    console.log(client);
    return this.httpClient.post(`${this.url}`, client)

    // return this.http.post(`${this.url}`, client).pipe(retry(3), catchError(this.handleError))
  }

  getClient(clientId) {
   return this.httpClient.get(`${this.url}/${clientId}`);
  }

  getClientVehicles(clientId) {
    return this.httpClient.get(`${this.urlclientvehicles}/client/${clientId}`)
  }

  ClientVehicles(clientId): void {
     this.httpClient.get<Vehicle[]>(`${this.urlclientvehicles}/client/${clientId}`).subscribe(data => {
      console.log(data)
      this.dataChangeVehicle.next(data);

      
    },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      });
  }

  getClientPolicies(clientId) {
    return this.httpClient.get(`${this.urlclientpolicies}/client/${clientId}`)
  }
}
