import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {Component, Inject} from '@angular/core';
import {ClientService} from '../../common/client.service';
import { HttpErrorResponse } from '@angular/common/http';


@Component({
  selector: 'app-deleteclient.dialog',
  templateUrl: '../../dialogs/delete/deleteclient.dialog.html',
  styleUrls: ['../../dialogs/delete/deleteclient.dialog.css']
})
export class DeleteClientComponent {
  errors: HttpErrorResponse[] = []

  constructor(public dialogRef: MatDialogRef<DeleteClientComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public clientService: ClientService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  // deleteVehicle(): void {
  //   const vehicleId = this.data.id
  //   this.clientService.deleteVehicle(vehicleId).subscribe(
  //   ()=>{console.log('Vehicle was deleted');     console.log(vehicleId)
  // },
  //   (errorResponse: HttpErrorResponse)=>{this.errors = errorResponse.error.errors; console.log(this.errors)});
  // }
}
