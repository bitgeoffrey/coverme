import {Component, Inject, OnInit, Input, ViewChild, ElementRef} from '@angular/core';
import {ClientService} from '../common/client.service';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';
import { MatDialog, MatSnackBar, MatPaginator, MatSort } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, fromEvent, merge, Observable } from 'rxjs';
import { map, debounceTime, distinctUntilChanged } from 'rxjs/operators';




import { Client } from '../common/client.model';
import { Vehicle } from '../../vehicle/common/vehicle.model';


@Component({
  selector: 'app-show.dialog',
  templateUrl: './showclient.html',
  styleUrls: ['./showclient.css']
})
export class ShowClientComponent implements OnInit{
  displayedColumns = ['regno', 'make', 'model', 'yom', 'value', 'actions'];
  clientvehicleData: ClientService | null;
  dataSource: ClientVehicleDataSource | null;
  index: number;
  id: string;

  errors: HttpErrorResponse[] = []

  client: Client
  clientvehicles: Vehicle[] = []
  clientpolicies:  []

  constructor( public clientService: ClientService, public dialog: MatDialog, private router: Router, private route: ActivatedRoute, private _snackBar: MatSnackBar, public httpClient: HttpClient,) { }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;

 

  refresh() {
    this.loadData();
  }


  ngOnInit() {
    this.route.params.subscribe(
      (params)=>{
        this.getClient(params['id']);
      }
    );
    this.loadData();
  }

  getClient(clientId) {
    this.clientService.getClient(clientId).subscribe(
      (data: Client)=>{
        this.client = data;
      },
      (errorResponse: HttpErrorResponse)=>{
        this._snackBar.open(errorResponse.error.errors[0].detail, '', { duration: 4000 , panelClass : 'error_snack' })
      }
    );
    this.getClientVehicles(clientId);
    this.getClientPolicies(clientId);
  }

  getClientVehicles(clientId) {
   this.clientService.getClientVehicles(clientId).subscribe(
     (data: Vehicle[])=>{
      this.clientvehicles = data;
     },
     (errorResponse: HttpErrorResponse)=>{
       this._snackBar.open(errorResponse.error.errors[0].detail, '', {duration: 4000, panelClass: 'error_snack'})
     }
   )   
  }

  getClientPolicies(clientId) {
    this.clientService.getClientPolicies(clientId).subscribe(
      (data: [])=>{
       this.clientpolicies = data;
      },
      (errorResponse: HttpErrorResponse)=>{
        this._snackBar.open(errorResponse.error.errors[0].detail, '', {duration: 4000, panelClass: 'error_snack'})
      }
    )   
   }

   public refreshTable() {
    this.paginator._changePageSize(this.paginator.pageSize);
  }


  public loadData() {
    this.clientvehicleData = new ClientService(this.httpClient);
    this.dataSource = new ClientVehicleDataSource(this.clientvehicleData, this.paginator, this.sort, this.route);
    fromEvent(this.filter.nativeElement, 'keyup')
      .pipe(debounceTime(150), distinctUntilChanged())
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }

        this.dataSource.filter = this.filter.nativeElement.value;
        // this.isLoadingResults = false
      });
  }

}


//Client`s Vehicle Data source
export class ClientVehicleDataSource extends DataSource<Vehicle> {
  _filterChange = new BehaviorSubject('');

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: Vehicle[] = [];

  renderedData: Vehicle[] = [];


  constructor(public _clientvehicleService: ClientService,
    public _paginator: MatPaginator,
    public _sort: MatSort,
    private route: ActivatedRoute,) {
    super();
    this._filterChange.subscribe(() => this._paginator.pageIndex = 0);
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<Vehicle[]> {

    console.log('reached')
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this._clientvehicleService.dataChangeVehicle,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];

    this.route.params.subscribe(
      (params)=>{
        console.log(params['id'])
        // this.getClientVehicles(params['id'])
        this._clientvehicleService.ClientVehicles(params['id']);
      }
    )   

    // this._clientvehicleService.getClientVehicles();


    


    return merge(...displayDataChanges).pipe(map(() => {
      this.filteredData = this._clientvehicleService.dataclientvehicle.slice().filter((vehicle: Vehicle) => {
        const searchStr = (vehicle.regno + vehicle.make + vehicle.model + vehicle.yom + vehicle.value).toLowerCase();
        return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
      });

      console.log(this.filteredData)

      // Sort filtered data
      const sortedData = this.sortData(this.filteredData.slice());

      // Grab the page's slice of the filtered sorted data.
      const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
      this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
      return this.renderedData;
    }
    ));
  }

  disconnect() { }


  /** Returns a sorted copy of the database data. */
    sortData(data: Vehicle[]): Vehicle[] {

    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';

      switch (this._sort.active) {
        case 'regno': [propertyA, propertyB] = [a.regno, b.regno]; break;
        case 'make': [propertyA, propertyB] = [a.make, b.make]; break;
        case 'model': [propertyA, propertyB] = [a.model, b.model]; break;
        case 'yom': [propertyA, propertyB] = [a.yom, b.yom]; break;
        case 'value': [propertyA, propertyB] = [a.value, b.value]; break;
        case 'createdAt': [propertyA, propertyB] = [a.createdAt, b.createdAt]; break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    });
  }
}
