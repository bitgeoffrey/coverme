import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  realTime: any;

  constructor() { }

  ngOnInit() {
    this.showTime();
    
    
  }

  showTime() {
    setInterval(
      ()=>{ this.realTime = Date.now(); }), 1000;
  }

}
