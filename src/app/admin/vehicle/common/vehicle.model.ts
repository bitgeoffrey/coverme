export class Vehicle {
    _id?: string;
    regno?: string;
    make?: string;
    model?: string;
    yom?: number;
    value?: string;
    client?: string;
    createdAt?: string;
  }
  