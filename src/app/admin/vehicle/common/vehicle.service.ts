import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Issue } from './issue.model';
import { Vehicle } from './vehicle.model'
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private readonly API_URL = 'https://api.github.com/repos/angular/angular/issues';
  private readonly url = 'http://localhost:3001/api/vehicles';


  dataChange: BehaviorSubject<Vehicle[]> = new BehaviorSubject<Vehicle[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;

  constructor(private httpClient: HttpClient) { }

  get data(): Vehicle[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  /** CRUD METHODS */
  getAllVehicles(): void {
    this.httpClient.get<Vehicle[]>(this.url).subscribe(data => {

      this.dataChange.next(data);
    },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      });
  }

  // DEMO ONLY, you can find working methods below
  // addIssue (issue: Issue): void {
  //   this.dialogData = issue;
  // }

  addVehicle(vehicle: Vehicle) {
    return this.httpClient.post(`${this.url}`, vehicle)
  }


  // updateIssue(issue: Issue): void {
  //   this.dialogData = issue;
  // }

  updateVehicle(vehicle: Vehicle, vehicleId) {
    console.log('see below id')
    console.log(vehicleId)
  
    return this.httpClient.post(`${this.url}/${vehicleId}`, vehicle)
  }

  deleteVehicle(vehicleId: string) {
    console.log(vehicleId);
    const deleteVehicle = {
      deleted: true
    }
    return this.httpClient.post(`${this.url}/remove/${vehicleId}`, deleteVehicle)
  }


}



/* REAL LIFE CRUD Methods I've used in my projects. ToasterService uses Material Toasts for displaying messages:

    // ADD, POST METHOD
    addItem(kanbanItem: KanbanItem): void {
    this.httpClient.post(this.API_URL, kanbanItem).subscribe(data => {
      this.dialogData = kanbanItem;
      this.toasterService.showToaster('Successfully added', 3000);
      },
      (err: HttpErrorResponse) => {
      this.toasterService.showToaster('Error occurred. Details: ' + err.name + ' ' + err.message, 8000);
    });
   }

    // UPDATE, PUT METHOD
     updateItem(kanbanItem: KanbanItem): void {
    this.httpClient.put(this.API_URL + kanbanItem.id, kanbanItem).subscribe(data => {
        this.dialogData = kanbanItem;
        this.toasterService.showToaster('Successfully edited', 3000);
      },
      (err: HttpErrorResponse) => {
        this.toasterService.showToaster('Error occurred. Details: ' + err.name + ' ' + err.message, 8000);
      }
    );
  }

  // DELETE METHOD
  deleteItem(id: number): void {
    this.httpClient.delete(this.API_URL + id).subscribe(data => {
      console.log(data['']);
        this.toasterService.showToaster('Successfully deleted', 3000);
      },
      (err: HttpErrorResponse) => {
        this.toasterService.showToaster('Error occurred. Details: ' + err.name + ' ' + err.message, 8000);
      }
    );
  }
*/




