import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject, OnInit } from '@angular/core';
import { DataService } from '../../common/vehicle.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import {MatSnackBar} from '@angular/material/snack-bar';


import { Issue } from '../../common/issue.model';

@Component({
  selector: 'app-add.dialog',
  templateUrl: '../../dialogs/add/add.dialog.html',
  styleUrls: ['../../dialogs/add/add.dialog.css']
})

export class AddDialogComponent implements OnInit{

  addVehicleFG: FormGroup

  errors: HttpErrorResponse[] = []

  form_validation_messages = {
    'regno': [
      { type: 'required', message: 'Vehicle Registartion Number is required' },
    ],
    'make': [
      { type: 'required', message: 'Vehicle Make is required' },
    ],
    'model': [
      { type: 'required', message: 'Vehicle Model is required' },
    ],
    'yom': [
      { type: 'required', message: 'Year of manufacture is required' },
    ],
    'value': [
      { type: 'required', message: 'Vehicle value is required' },
    ]
  }

  constructor(private fb: FormBuilder, private vehicleService: DataService, public dialogRef: MatDialogRef<AddDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Issue,
    public dataService: DataService, public _snackBar: MatSnackBar) { }

ngOnInit() {
  this.addVehicleFG = this.fb.group({
    regno: ['', Validators.required],
    make: ['', Validators.required],
    model: ['', Validators.required],
    yom: ['', Validators.required],
    value: ['', Validators.required]
  })
}

  addVehicle() {
    this.vehicleService.addVehicle(this.addVehicleFG.value).subscribe(
      ()=>{
        this._snackBar.open("Success Vehicle was added", '', { duration: 4000, panelClass : 'success_snack' })
      },
      (errorResponse: HttpErrorResponse)=>{
        this.errors = errorResponse.error.errors;
        this._snackBar.open(errorResponse.error.errors[0].detail, '', { duration: 4000 , panelClass : 'error_snack' })
      }
    );
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  // public confirmAdd(): void {
  //   this.dataService.addVehicle(this.data);
  // }
}
