import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {Component, Inject} from '@angular/core';
import {DataService} from '../../common/vehicle.service';
import { HttpErrorResponse } from '@angular/common/http';


@Component({
  selector: 'app-delete.dialog',
  templateUrl: '../../dialogs/delete/delete.dialog.html',
  styleUrls: ['../../dialogs/delete/delete.dialog.css']
})
export class DeleteDialogComponent {
  errors: HttpErrorResponse[] = []

  constructor(public dialogRef: MatDialogRef<DeleteDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public vehicleService: DataService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  deleteVehicle(): void {
    const vehicleId = this.data.id
    this.vehicleService.deleteVehicle(vehicleId).subscribe(
    ()=>{console.log('Vehicle was deleted');     console.log(vehicleId)
  },
    (errorResponse: HttpErrorResponse)=>{this.errors = errorResponse.error.errors; console.log(this.errors)});
  }
}
