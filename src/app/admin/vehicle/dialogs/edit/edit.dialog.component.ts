import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {Component, Inject, OnInit} from '@angular/core';
import {DataService} from '../../common/vehicle.service';
import {FormGroup ,FormControl, Validators, FormBuilder} from '@angular/forms';

import { HttpErrorResponse } from '@angular/common/http';


@Component({
  selector: 'app-baza.dialog',
  templateUrl: '../../dialogs/edit/edit.dialog.html',
  styleUrls: ['../../dialogs/edit/edit.dialog.css']
})
export class EditDialogComponent implements OnInit{

  editVehicleFG: FormGroup

  errors: HttpErrorResponse[] = []

  form_validation_messages = {
    'regno': [
      { type: 'required', message: 'Vehicle Registartion Number is required' },
    ],
    'make': [
      { type: 'required', message: 'Vehicle Make is required' },
    ],
    'model': [
      { type: 'required', message: 'Vehicle Model is required' },
    ],
    'yom': [
      { type: 'required', message: 'Year of manufacture is required' },
    ],
    'value': [
      { type: 'required', message: 'Vehicle value is required' },
    ]
  }

  constructor(private fb: FormBuilder, private vehicleService: DataService, public dialogRef: MatDialogRef<EditDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);

  ngOnInit() {
    this.editVehicleFG = this.fb.group({
      // regno: ['', Validators.required],
      make: ['', Validators.required],
      model: ['', Validators.required],
      yom: ['', Validators.required],
      value: ['', Validators.required]
    })
  }

  editVehicle() {
    const vehicleId = this.data.id
    this.vehicleService.updateVehicle(this.editVehicleFG.value, vehicleId).subscribe(
    ()=>{console.log('Vehicle was edited');     console.log(vehicleId)
  },
    (errorResponse: HttpErrorResponse)=>{this.errors = errorResponse.error.errors; console.log(this.errors)});
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  // stopEdit(): void {
  //   this.dataService.updateIssue(this.data);
  // }
}
