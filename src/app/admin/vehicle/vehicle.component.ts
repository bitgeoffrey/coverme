import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DataService } from './common/vehicle.service';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Issue } from './common/issue.model';
import { Vehicle } from './common/vehicle.model';
import { DataSource } from '@angular/cdk/collections';
import { AddDialogComponent } from './dialogs/add/add.dialog.component';
import { ShowDialogComponent } from './dialogs/show/show.dialog.component';
import { EditDialogComponent } from './dialogs/edit/edit.dialog.component';
import { DeleteDialogComponent } from './dialogs/delete/delete.dialog.component';
import { BehaviorSubject, fromEvent, merge, Observable } from 'rxjs';
import { map, debounceTime, distinctUntilChanged } from 'rxjs/operators';


@Component({
  selector: 'app-vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.css']
})

export class VehicleComponent implements OnInit {
  displayedColumns = ['regno', 'make', 'model', 'yom', 'value', 'actions'];

  vehicleData: DataService | null;
  dataSource: VehicleDataSource | null;
  index: number;
  id: string;

  // isLoadingResults = true;


  constructor(public httpClient: HttpClient,
    public dialog: MatDialog,
    public dataService: DataService) { }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;

  ngOnInit() {
    this.loadData();
  }

  refresh() {
    this.loadData();
  }

  addVehicle(issue: Issue) {
    const dialogRef = this.dialog.open(AddDialogComponent, {
      data: { issue: issue }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        // After dialog is closed we're doing frontend updates
        // For add we're just pushing a new row inside DataService
        this.vehicleData.dataChange.value.push(this.dataService.getDialogData());
        this.refreshTable();
      }
    });
  }

  editVehicle(i: number, _id: string, regno: string, make: string, model: string, yom: string, value: string, createdAt: string) {
    // this.id = _id;
    // index row is used just for debugging proposes and can be removed
    this.index = i;
    // console.log(this.index);
    // console.log(_id)
    const dialogRef = this.dialog.open(EditDialogComponent, {
      data: { id: _id, regno: regno, make: make, model: model, yom: yom, value: value},
      
    });


    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        // When using an edit things are little different, firstly we find record inside DataService by id
        const foundIndex = this.vehicleData.dataChange.value.findIndex(x => x._id === this.id);
        console.log(foundIndex)
        // Then you update that record using data from dialogData (values you enetered)
        this.vehicleData.dataChange.value[foundIndex] = this.dataService.getDialogData();
        // And lastly refresh table
        this.refreshTable();
      }
    });
  }

  showVehicle(i: number, _id: string, regno: string, make: string, model: string, yom: string, value: string, createdAt: string, client: any) {
    this.index = i;
    // this.id = _id;
    const dialogRef = this.dialog.open(ShowDialogComponent, {
      data: { id: _id, regno: regno, make: make, model: model, yom: yom, value: value, created: createdAt, owner: client }
    });

    console.log(dialogRef)


    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        // const foundIndex = this.vehicleData.dataChange.value.findIndex(x => x._id === this.id);
        // this.vehicleData.dataChange.value.splice(foundIndex, 1);
        this.refreshTable();
      }
    });
  }

  deleteVehicle(i: number, _id: string, regno: string, make: string, model: string, yom: string, value: string) {
    this.index = i;
    // this.id = _id;
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      data: { id: _id, regno: regno, make: make, model: model, yom: yom, value: value }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        const foundIndex = this.vehicleData.dataChange.value.findIndex(x => x._id === this.id);
        // for delete we use splice in order to remove single object from DataService
        this.vehicleData.dataChange.value.splice(foundIndex, 1);
        this.refreshTable();
      }
    });
  }


  public refreshTable() {
    this.paginator._changePageSize(this.paginator.pageSize);
  }


  /*   // If you don't need a filter or a pagination this can be simplified, you just use code from else block
    // OLD METHOD:
    // if there's a paginator active we're using it for refresh
    if (this.dataSource._paginator.hasNextPage()) {
      this.dataSource._paginator.nextPage();
      this.dataSource._paginator.previousPage();
      // in case we're on last page this if will tick
    } else if (this.dataSource._paginator.hasPreviousPage()) {
      this.dataSource._paginator.previousPage();
      this.dataSource._paginator.nextPage();
      // in all other cases including active filter we do it like this
    } else {
      this.dataSource.filter = '';
      this.dataSource.filter = this.filter.nativeElement.value;
    }*/



  public loadData() {
    this.vehicleData = new DataService(this.httpClient);
    this.dataSource = new VehicleDataSource(this.vehicleData, this.paginator, this.sort);
    fromEvent(this.filter.nativeElement, 'keyup')
      .pipe(debounceTime(150), distinctUntilChanged())
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }

        this.dataSource.filter = this.filter.nativeElement.value;
        // this.isLoadingResults = false
      });
  }
}


//Vehicle Data source

export class VehicleDataSource extends DataSource<Vehicle> {
  _filterChange = new BehaviorSubject('');

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: Vehicle[] = [];

  renderedData: Vehicle[] = [];


  constructor(public _vehicleService: DataService,
    public _paginator: MatPaginator,
    public _sort: MatSort) {
    super();
    // Reset to the first page when the user changes the filter.
    this._filterChange.subscribe(() => this._paginator.pageIndex = 0);
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<Vehicle[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this._vehicleService.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];
    this._vehicleService.getAllVehicles();


    return merge(...displayDataChanges).pipe(map(() => {
      this.filteredData = this._vehicleService.data.slice().filter((vehicle: Vehicle) => {
        const searchStr = (vehicle.regno + vehicle.make + vehicle.model + vehicle.yom + vehicle.value).toLowerCase();
        return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
      });

      // Sort filtered data
      const sortedData = this.sortData(this.filteredData.slice());

      // Grab the page's slice of the filtered sorted data.
      const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
      this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
      return this.renderedData;
    }
    ));
  }

  disconnect() { }


  /** Returns a sorted copy of the database data. */
    sortData(data: Vehicle[]): Vehicle[] {

    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';

      switch (this._sort.active) {
        case 'regno': [propertyA, propertyB] = [a.regno, b.regno]; break;
        case 'make': [propertyA, propertyB] = [a.make, b.make]; break;
        case 'model': [propertyA, propertyB] = [a.model, b.model]; break;
        case 'yom': [propertyA, propertyB] = [a.yom, b.yom]; break;
        case 'value': [propertyA, propertyB] = [a.value, b.value]; break;
        case 'createdAt': [propertyA, propertyB] = [a.createdAt, b.createdAt]; break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    });
  }
}
