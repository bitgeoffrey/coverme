import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {MatSidenavModule, MatButtonModule, MatIconModule, MatToolbarModule, MatListModule, MatDividerModule, MatMenuModule, MatCardModule, MatTabsModule,
  MatChipsModule, MatTableModule, MatFormFieldModule, MatPaginatorModule, MatInputModule, MatSortModule, MatDialogModule, MatProgressSpinnerModule, MatSnackBarModule}  from '@angular/material';




@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatSidenavModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    MatListModule,
    MatDividerModule,
    MatMenuModule,
    MatCardModule,
    MatTabsModule,
    MatChipsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatSnackBarModule
    
  ],

  exports: [
    MatSidenavModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    MatListModule,
    MatDividerModule,
    MatMenuModule,
    MatCardModule,
    MatTabsModule,
    MatChipsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatSnackBarModule
  ]
})
export class AppMaterialModule { }
