import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { AppMaterialModule } from '../app/app-material.module';
import { FlexLayoutModule } from '@angular/flex-layout';


import { AdminComponent } from './admin/admin.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';


const routes: Routes = [
  { path: '', redirectTo: '/admin/dashboard', pathMatch: 'full' },
  {
    path: 'admin', component: AdminComponent, children: [
      { path: 'dashboard', component: DashboardComponent }
    ]
  }
];

@NgModule({
  declarations: [AdminComponent, DashboardComponent],
  imports: [RouterModule.forRoot(routes), AppMaterialModule, CommonModule, FlexLayoutModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
