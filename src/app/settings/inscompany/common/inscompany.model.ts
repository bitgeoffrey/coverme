export class InsCompany {
    _id?: string;
    name?: string;
    location?: string;
    address?: string;
    phone?: string;
    email?: string;
    contactpersons?: [];
    policies?: [];
    createdAt?: string;
  }
  