import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { InsCompany } from './inscompany.model'
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class InsCompanyService {
  private readonly url = 'http://localhost:3002/api/inscompany';


  dataChange: BehaviorSubject<InsCompany[]> = new BehaviorSubject<InsCompany[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;

  constructor(private httpClient: HttpClient) { }

  get data(): InsCompany[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  getAllCompanies(): void {
    this.httpClient.get<InsCompany[]>(this.url).subscribe(data => {

      this.dataChange.next(data);
    },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      });
  }


  addCompany(company: InsCompany) {
    return this.httpClient.post(`${this.url}`, company)
  }


  updateCompany(company: InsCompany, companyId) {
  
    return this.httpClient.post(`${this.url}/${companyId}`, company)
  }

  deleteCompany(companyId: string) {
    const deletecompany = {
      deleted: true
    }
    return this.httpClient.post(`${this.url}/remove/${companyId}`, deletecompany)
  }
}
