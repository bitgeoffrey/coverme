import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject, OnInit } from '@angular/core';
import { InsCompanyService } from '../../common/inscompany.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import {MatSnackBar} from '@angular/material/snack-bar';
import { InsCompany } from '../../common/inscompany.model';

@Component({
  selector: 'app-add.dialog',
  templateUrl: '../../dialogs/add/add.dialog.html',
  styleUrls: ['../../dialogs/add/add.dialog.css']
})

export class AddDialogComponent implements OnInit{

  addCompanyFG: FormGroup

  errors: HttpErrorResponse[] = []

  form_validation_messages = {
    'name': [
      { type: 'required', message: 'Insurance Company name is required' },
    ],

    'email': [
      { type: 'required', message: 'Email is required' },
      { type: 'pattern', message: 'Enter a valid email' }
    ],
    'phone': [
      { type: 'required', message: 'Phone number is required' },
      { type: 'pattern', message: 'Enter a valid phone number' }
    ]
  }

  constructor(private fb: FormBuilder, private inscompanyService: InsCompanyService, public dialogRef: MatDialogRef<AddDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: InsCompany, public _snackBar: MatSnackBar) { }

ngOnInit() {
  this.addCompanyFG = this.fb.group({
    name: ['', Validators.required],
    email: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$')]],
    phone: ['', [ Validators.required, Validators.pattern('^[0-9]{10}$')] ],
    location: [''],
    address: [''],
  })
}

addCompany() {
  this.inscompanyService.addCompany(this.addCompanyFG.value).subscribe(
    ()=>{
      this._snackBar.open("Success Insurance Company was added", '', { duration: 4000, panelClass : 'success_snack' })
    },
    (errorResponse: HttpErrorResponse)=>{
      this.errors = errorResponse.error.errors;
      this._snackBar.open(errorResponse.error.errors[0].detail, '', { duration: 4000 , panelClass : 'error_snack' })
    }
  );
}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
