import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {Component, Inject, OnInit} from '@angular/core';
import { InsCompanyService } from '../../common/inscompany.service';
import {FormGroup, Validators, FormBuilder} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';


import { HttpErrorResponse } from '@angular/common/http';


@Component({
  selector: 'app-baza.dialog',
  templateUrl: '../../dialogs/edit/edit.dialog.html',
  styleUrls: ['../../dialogs/edit/edit.dialog.css']
})
export class EditDialogComponent implements OnInit{

  editCompanyFG: FormGroup

  errors: HttpErrorResponse[] = []

  form_validation_messages = {

    'name': [
      { type: 'required', message: 'Insurance Company name is required' },
    ],

    'email': [
      { type: 'required', message: 'Email is required' },
      { type: 'pattern', message: 'Enter a valid email' }
    ],
    'phone': [
      { type: 'required', message: 'Phone number is required' },
      { type: 'pattern', message: 'Enter a valid phone number' }
    ]
  }

  constructor(private fb: FormBuilder, private inscompanyService: InsCompanyService, public dialogRef: MatDialogRef<EditDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public _snackBar: MatSnackBar) { }

  ngOnInit() {
    this.editCompanyFG = this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$')]],
      phone: ['', [ Validators.required, Validators.pattern('^[0-9]{10}$')] ],
      location: [''],
      address: [''],
    })
  }

  editCompany() {
    const benefitId = this.data.id
    this.inscompanyService.updateCompany(this.editCompanyFG.value, benefitId).subscribe(
      ()=>{
        this._snackBar.open("Success Insurance company was updated", '', { duration: 4000, panelClass : 'success_snack' })
      },
    (errorResponse: HttpErrorResponse)=>{errorResponse.error.errors[0].detail, '', { duration: 4000 , panelClass : 'error_snack' }});
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
