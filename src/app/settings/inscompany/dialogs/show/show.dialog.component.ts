import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {Component, Inject} from '@angular/core';
import { InsCompanyService } from '../../common/inscompany.service';
import { HttpErrorResponse } from '@angular/common/http';


@Component({
  selector: 'app-show.dialog',
  templateUrl: '../../dialogs/show/show.dialog.html',
  styleUrls: ['../../dialogs/show/show.dialog.css']
})
export class ShowDialogComponent {
  errors: HttpErrorResponse[] = []

  constructor(public dialogRef: MatDialogRef<ShowDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public inscompanyService: InsCompanyService) { }

}
