import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InscompanyComponent } from './inscompany.component';

describe('InscompanyComponent', () => {
  let component: InscompanyComponent;
  let fixture: ComponentFixture<InscompanyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InscompanyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InscompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
