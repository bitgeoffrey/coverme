import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { InsCompanyService } from './common/inscompany.service';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { InsCompany } from './common/inscompany.model';
import { DataSource } from '@angular/cdk/collections';
import { AddDialogComponent } from './dialogs/add/add.dialog.component';
import { ShowDialogComponent } from './dialogs/show/show.dialog.component';
import { EditDialogComponent } from './dialogs/edit/edit.dialog.component';
import { DeleteDialogComponent } from './dialogs/delete/delete.dialog.component';
import { BehaviorSubject, fromEvent, merge, Observable } from 'rxjs';
import { map, debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-inscompany',
  templateUrl: './inscompany.component.html',
  styleUrls: ['./inscompany.component.css']
})

export class InscompanyComponent implements OnInit {

  displayedColumns = ['name', 'phone', 'email', 'location', 'address', 'contactpersons', 'policies', 'actions'];

  companyData: InsCompanyService | null;
  dataSource: CompanyDataSource | null;
  index: number;
  id: string;

  constructor(public httpClient: HttpClient, public dialog: MatDialog, public dataService: InsCompanyService) { }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;

  ngOnInit() {
    this.loadData();
  }

  refresh() {
    this.loadData();
  }


  addCompany(company: InsCompany) {
    const dialogRef = this.dialog.open(AddDialogComponent, {
      data: { company: company }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        // After dialog is closed we're doing frontend updates
        // For add we're just pushing a new row inside DataService
        this.companyData.dataChange.value.push(this.dataService.getDialogData());
        this.refreshTable();
      }
    });
  }

  editCompany(i: number, _id: string, name: string, location: string, address: string, phone: string, email:string) {
    const dialogRef = this.dialog.open(EditDialogComponent, {
      data: { id: _id, location: location, name: name, address: address, phone: phone, email: email},
      
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        const foundIndex = this.companyData.dataChange.value.findIndex(x => x._id === this.id);
  
        this.companyData.dataChange.value[foundIndex] = this.dataService.getDialogData();
        this.refreshTable();
      }
    });
  }

  showCompany(i: number, _id: string, name: string, location: string, address: string, phone: string, email:string, createdAt: string, policies: [], contactpersons:[]) {
    this.index = i;
    
    const dialogRef = this.dialog.open(ShowDialogComponent, {
      data: { id: _id, location: location, name: name, address: address, phone: phone, email: email, policies: policies, contactpersons: contactpersons, created: createdAt}
    });


    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {

        this.refreshTable();
      }
    });
  }

  deleteCompany(i: number, _id: string, name: string, location: string, phone: string, email:string,) {
    this.index = i;
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      data: { id: _id, name: name, location:location, phone: phone, email: email }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        const foundIndex = this.companyData.dataChange.value.findIndex(x => x._id === this.id);
        this.companyData.dataChange.value.splice(foundIndex, 1);
        this.refreshTable();
      }
    });
  }


  public refreshTable() {
    this.paginator._changePageSize(this.paginator.pageSize);
  }

  public loadData() {
    this.companyData = new InsCompanyService(this.httpClient);
    this.dataSource = new CompanyDataSource(this.companyData, this.paginator, this.sort);
    fromEvent(this.filter.nativeElement, 'keyup')
      .pipe(debounceTime(150), distinctUntilChanged())
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }
}


//Vehicle Data source

export class CompanyDataSource extends DataSource<InsCompany> {
  _filterChange = new BehaviorSubject('');

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: InsCompany[] = [];

  renderedData: InsCompany[] = [];


  constructor(public _inscompanyService: InsCompanyService,
    public _paginator: MatPaginator,
    public _sort: MatSort) {
    super();
    // Reset to the first page when the user changes the filter.
    this._filterChange.subscribe(() => this._paginator.pageIndex = 0);
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<InsCompany[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this._inscompanyService.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];
    this._inscompanyService.getAllCompanies();


    return merge(...displayDataChanges).pipe(map(() => {
      this.filteredData = this._inscompanyService.data.slice().filter((type: InsCompany) => {
        const searchStr = (type.location + type.name + type.address + type.phone + type.email).toLowerCase();
        return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
      });

      // Sort filtered data
      const sortedData = this.sortData(this.filteredData.slice());

      // Grab the page's slice of the filtered sorted data.
      const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
      this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
      return this.renderedData;
    }
    ));
  }

  disconnect() { }


  /** Returns a sorted copy of the database data. */
    sortData(data: InsCompany[]): InsCompany[] {

    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';

      switch (this._sort.active) {
        case 'location': [propertyA, propertyB] = [a.location, b.location]; break;
        case 'address': [propertyA, propertyB] = [a.address, b.address]; break;
        case 'email': [propertyA, propertyB] = [a.email, b.email]; break;
        case 'name': [propertyA, propertyB] = [a.name, b.name]; break;
        case 'createdAt': [propertyA, propertyB] = [a.createdAt, b.createdAt]; break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    });
  }
}


