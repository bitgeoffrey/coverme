import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Policybenefit } from './policybenefit.model'
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PolicybenefitService {
  private readonly url = 'http://localhost:3002/api/policybenefits';


  dataChange: BehaviorSubject<Policybenefit[]> = new BehaviorSubject<Policybenefit[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;

  constructor(private httpClient: HttpClient) { }

  get data(): Policybenefit[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  getAllBenefits(): void {
    this.httpClient.get<Policybenefit[]>(this.url).subscribe(data => {

      this.dataChange.next(data);
    },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      });
  }


  addBenefit(benefit: Policybenefit) {
    return this.httpClient.post(`${this.url}`, benefit)
  }


  updateBenefit(benefit: Policybenefit, benefitId) {
  
    return this.httpClient.post(`${this.url}/${benefitId}`, benefit)
  }

  deleteBenefit(benefitId: string) {
    const deletebenefit = {
      deleted: true
    }
    return this.httpClient.post(`${this.url}/remove/${benefitId}`, deletebenefit)
  }
}
