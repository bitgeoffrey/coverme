import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject, OnInit } from '@angular/core';
import { PolicybenefitService } from '../../common/policybenefit.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import {MatSnackBar} from '@angular/material/snack-bar';
import { Policybenefit } from '../../common/policybenefit.model';

@Component({
  selector: 'app-add.dialog',
  templateUrl: '../../dialogs/add/add.dialog.html',
  styleUrls: ['../../dialogs/add/add.dialog.css']
})

export class AddDialogComponent implements OnInit{

  addBenefitFG: FormGroup

  errors: HttpErrorResponse[] = []

  form_validation_messages = {
    'name': [
      { type: 'required', message: 'Policy type name is required' },
    ],
  }

  constructor(private fb: FormBuilder, private benefitService: PolicybenefitService, public dialogRef: MatDialogRef<AddDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Policybenefit, public _snackBar: MatSnackBar) { }

ngOnInit() {
  this.addBenefitFG = this.fb.group({
    name: ['', Validators.required],
  })
}

addBenefit() {
  this.benefitService.addBenefit(this.addBenefitFG.value).subscribe(
    ()=>{
      this._snackBar.open("Success Policy Benefit was added", '', { duration: 4000, panelClass : 'success_snack' })
    },
    (errorResponse: HttpErrorResponse)=>{
      this.errors = errorResponse.error.errors;
      this._snackBar.open(errorResponse.error.errors[0].detail, '', { duration: 4000 , panelClass : 'error_snack' })
    }
  );
}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
