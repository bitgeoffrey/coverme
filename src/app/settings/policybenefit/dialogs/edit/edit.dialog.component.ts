import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {Component, Inject, OnInit} from '@angular/core';
import { PolicybenefitService } from '../../common/policybenefit.service';
import {FormGroup ,FormControl, Validators, FormBuilder} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';


import { HttpErrorResponse } from '@angular/common/http';


@Component({
  selector: 'app-baza.dialog',
  templateUrl: '../../dialogs/edit/edit.dialog.html',
  styleUrls: ['../../dialogs/edit/edit.dialog.css']
})
export class EditDialogComponent implements OnInit{

  editBenefitFG: FormGroup

  errors: HttpErrorResponse[] = []

  form_validation_messages = {

    'name': [
      { type: 'required', message: 'Policy type  name is required' },
    ]
  }

  constructor(private fb: FormBuilder, private benefitService: PolicybenefitService, public dialogRef: MatDialogRef<EditDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public _snackBar: MatSnackBar) { }

  ngOnInit() {
    this.editBenefitFG = this.fb.group({
      name: ['', Validators.required],
      // code: ['', Validators.required],  {value: '', disabled: true}
    })
  }

  editBenefit() {
    const benefitId = this.data.id
    this.benefitService.updateBenefit(this.editBenefitFG.value, benefitId).subscribe(
      ()=>{
        this._snackBar.open("Success Policy Benefit was updated", '', { duration: 4000, panelClass : 'success_snack' })
      },
    (errorResponse: HttpErrorResponse)=>{errorResponse.error.errors[0].detail, '', { duration: 4000 , panelClass : 'error_snack' }});
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
