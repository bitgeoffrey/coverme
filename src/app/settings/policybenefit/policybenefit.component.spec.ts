import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PolicybenefitComponent } from './policybenefit.component';

describe('PolicybenefitComponent', () => {
  let component: PolicybenefitComponent;
  let fixture: ComponentFixture<PolicybenefitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PolicybenefitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PolicybenefitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
