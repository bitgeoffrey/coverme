import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { PolicybenefitService } from './common/policybenefit.service';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Policybenefit } from './common/policybenefit.model';
import { DataSource } from '@angular/cdk/collections';
import { AddDialogComponent } from './dialogs/add/add.dialog.component';
import { ShowDialogComponent } from './dialogs/show/show.dialog.component';
import { EditDialogComponent } from './dialogs/edit/edit.dialog.component';
import { DeleteDialogComponent } from './dialogs/delete/delete.dialog.component';
import { BehaviorSubject, fromEvent, merge, Observable } from 'rxjs';
import { map, debounceTime, distinctUntilChanged } from 'rxjs/operators';
@Component({
  selector: 'app-policybenefit',
  templateUrl: './policybenefit.component.html',
  styleUrls: ['./policybenefit.component.css']
})
export class PolicybenefitComponent implements OnInit {

  displayedColumns = ['name', 'actions'];

  benefitData: PolicybenefitService | null;
  dataSource: BenefitDataSource | null;
  index: number;
  id: string;

  constructor(public httpClient: HttpClient, public dialog: MatDialog, public dataService: PolicybenefitService) { }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;

  ngOnInit() {
    this.loadData();
  }

  refresh() {
    this.loadData();
  }


  addBenefit(benefit: Policybenefit) {
    const dialogRef = this.dialog.open(AddDialogComponent, {
      data: { benefit: benefit }
    });

    this.refreshTable();

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.benefitData.dataChange.value.push(this.dataService.getDialogData());
        // this.refreshTable();
      }
    });
  }

  editBenefit(i: number, _id: string, name: string) {
    const dialogRef = this.dialog.open(EditDialogComponent, {
      data: { id: _id, name: name},
      
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        const foundIndex = this.benefitData.dataChange.value.findIndex(x => x._id === this.id);
        this.benefitData.dataChange.value[foundIndex] = this.dataService.getDialogData();
        this.refreshTable();
      }
    });
  }

  showBenefit(i: number, _id: string, name: string, createdAt: string,) {
    this.index = i;
    
    const dialogRef = this.dialog.open(ShowDialogComponent, {
      data: { id: _id, name: name, created: createdAt}
    });


    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {

        this.refreshTable();
      }
    });
  }

  deleteBenefit(i: number, _id: string, name: string) {
    this.index = i;
    
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      data: { id: _id, name: name }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        const foundIndex = this.benefitData.dataChange.value.findIndex(x => x._id === this.id);
        this.benefitData.dataChange.value.splice(foundIndex, 1);
        this.refreshTable();
      }
    });
  }


  public refreshTable() {
    this.paginator._changePageSize(this.paginator.pageSize);
  }

  public loadData() {
    this.benefitData = new PolicybenefitService(this.httpClient);
    this.dataSource = new BenefitDataSource(this.benefitData, this.paginator, this.sort);
    fromEvent(this.filter.nativeElement, 'keyup')
      .pipe(debounceTime(150), distinctUntilChanged())
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }
}


//Vehicle Data source

export class BenefitDataSource extends DataSource<Policybenefit> {
  _filterChange = new BehaviorSubject('');

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: Policybenefit[] = [];

  renderedData: Policybenefit[] = [];


  constructor(public _benefitService: PolicybenefitService,
    public _paginator: MatPaginator,
    public _sort: MatSort) {
    super();
    // Reset to the first page when the user changes the filter.
    this._filterChange.subscribe(() => this._paginator.pageIndex = 0);
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<Policybenefit[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this._benefitService.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];
    this._benefitService.getAllBenefits();


    return merge(...displayDataChanges).pipe(map(() => {
      this.filteredData = this._benefitService.data.slice().filter((benefit: Policybenefit) => {
        const searchStr = (benefit.name).toLowerCase();
        return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
      });

      // Sort filtered data
      const sortedData = this.sortData(this.filteredData.slice());

      // Grab the page's slice of the filtered sorted data.
      const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
      this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
      return this.renderedData;
    }
    ));
  }

  disconnect() { }


  /** Returns a sorted copy of the database data. */
    sortData(data: Policybenefit[]): Policybenefit[] {

    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';

      switch (this._sort.active) {
        case 'name': [propertyA, propertyB] = [a.name, b.name]; break;
        case 'createdAt': [propertyA, propertyB] = [a.createdAt, b.createdAt]; break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    });
  }
}


