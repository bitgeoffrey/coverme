import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import {FormsModule  ,ReactiveFormsModule } from '@angular/forms';
import { AppMaterialModule } from '../../app-material.module';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AdminComponent } from '../../admin/admin.component';
import { PolicybenefitComponent } from './policybenefit.component';

import {MAT_DIALOG_DEFAULT_OPTIONS} from '@angular/material/dialog';

import { AddDialogComponent } from './dialogs/add/add.dialog.component'
import { EditDialogComponent } from './dialogs/edit/edit.dialog.component';
import { DeleteDialogComponent } from './dialogs/delete/delete.dialog.component';
import { ShowDialogComponent } from './dialogs/show/show.dialog.component';

const routes: Routes = [
  {
    path: 'admin', component: AdminComponent, children: [
      { path: 'policybenefits', component: PolicybenefitComponent },
    ]
  }
]

@NgModule({
  declarations: [
    PolicybenefitComponent,
    AddDialogComponent,
    EditDialogComponent,
    DeleteDialogComponent,
    ShowDialogComponent
  ],
  imports: [
    RouterModule.forRoot(routes),
    CommonModule,
    AppMaterialModule,
    ReactiveFormsModule,
    FormsModule,
    FlexLayoutModule
  ],
  entryComponents: [
    AddDialogComponent,
    EditDialogComponent,
    DeleteDialogComponent,
    ShowDialogComponent
  ],
  providers: [
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: true}}
  ]

})
export class PolicybenefitModule { }
