export class Policytype {
    _id?: string;
    name?: string;
    code?: string;
    policies?: [];
    createdAt?: string;
  }
  