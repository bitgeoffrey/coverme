import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject, OnInit } from '@angular/core';
import { PolicytypeService } from '../../common/policytype.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import {MatSnackBar} from '@angular/material/snack-bar';
import { Policytype } from '../../common/policytype.model';

@Component({
  selector: 'app-add.dialog',
  templateUrl: '../../dialogs/add/add.dialog.html',
  styleUrls: ['../../dialogs/add/add.dialog.css']
})

export class AddDialogComponent implements OnInit{

  addTypeFG: FormGroup

  errors: HttpErrorResponse[] = []

  form_validation_messages = {
    'code': [
      { type: 'required', message: 'Policy type code required' },
    ],
    'name': [
      { type: 'required', message: 'Policy type name is required' },
    ],
  }

  constructor(private fb: FormBuilder, private typeService: PolicytypeService, public dialogRef: MatDialogRef<AddDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Policytype, public _snackBar: MatSnackBar) { }

ngOnInit() {
  this.addTypeFG = this.fb.group({
    code: ['', Validators.required],
    name: ['', Validators.required],
  })
}

  addType() {
    this.typeService.addType(this.addTypeFG.value).subscribe(
      ()=>{
        this._snackBar.open("Success Policy type name was added", '', { duration: 4000, panelClass : 'success_snack' })
      },
      (errorResponse: HttpErrorResponse)=>{
        this.errors = errorResponse.error.errors;
        this._snackBar.open(errorResponse.error.errors[0].detail, '', { duration: 4000 , panelClass : 'error_snack' })
      }
    );
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  // public confirmAdd(): void {
  //   this.dataService.addVehicle(this.data);
  // }
}
