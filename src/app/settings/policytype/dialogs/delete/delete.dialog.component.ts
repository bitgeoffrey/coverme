import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {Component, Inject} from '@angular/core';
import { PolicytypeService } from '../../common/policytype.service';
import { HttpErrorResponse } from '@angular/common/http';
import {MatSnackBar} from '@angular/material/snack-bar';



@Component({
  selector: 'app-delete.dialog',
  templateUrl: '../../dialogs/delete/delete.dialog.html',
  styleUrls: ['../../dialogs/delete/delete.dialog.css']
})
export class DeleteDialogComponent {
  errors: HttpErrorResponse[] = []

  constructor(public dialogRef: MatDialogRef<DeleteDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any, public typeService: PolicytypeService, public _snackBar: MatSnackBar) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  deleteType(): void {
    const typeId = this.data.id
    this.typeService.deleteType(typeId).subscribe(
    ()=>{
      this._snackBar.open("Success Policy type name was deleted", '', { duration: 4000, panelClass : 'success_snack' })    },

    (errorResponse: HttpErrorResponse)=>{errorResponse.error.errors[0].detail, '', { duration: 4000 , panelClass : 'error_snack' }});
  }
}
