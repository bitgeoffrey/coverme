import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { PolicytypeService } from './common/policytype.service';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Policytype } from './common/policytype.model';
import { DataSource } from '@angular/cdk/collections';
import { AddDialogComponent } from './dialogs/add/add.dialog.component';
import { ShowDialogComponent } from './dialogs/show/show.dialog.component';
import { EditDialogComponent } from './dialogs/edit/edit.dialog.component';
import { DeleteDialogComponent } from './dialogs/delete/delete.dialog.component';
import { BehaviorSubject, fromEvent, merge, Observable } from 'rxjs';
import { map, debounceTime, distinctUntilChanged } from 'rxjs/operators';
@Component({
  selector: 'app-policytype',
  templateUrl: './policytype.component.html',
  styleUrls: ['./policytype.component.css']
})
export class PolicytypeComponent implements OnInit {

  displayedColumns = ['code' ,'name', 'policies', 'actions'];

  typeData: PolicytypeService | null;
  dataSource: TypeDataSource | null;
  index: number;
  id: string;

  constructor(public httpClient: HttpClient,
    public dialog: MatDialog,
    public dataService: PolicytypeService) { }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;

  ngOnInit() {
    this.loadData();
  }

  refresh() {
    this.loadData();
  }


  addType(type: Policytype) {
    const dialogRef = this.dialog.open(AddDialogComponent, {
      data: { type: type }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        // After dialog is closed we're doing frontend updates
        // For add we're just pushing a new row inside DataService
        this.typeData.dataChange.value.push(this.dataService.getDialogData());
        this.refreshTable();
      }
    });
  }

  editType(i: number, _id: string, code: string, name: string) {
    const dialogRef = this.dialog.open(EditDialogComponent, {
      data: { id: _id, code: code, name: name},
      
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        // When using an edit things are little different, firstly we find record inside DataService by id
        const foundIndex = this.typeData.dataChange.value.findIndex(x => x._id === this.id);
  
        // Then you update that record using data from dialogData (values you enetered)
        this.typeData.dataChange.value[foundIndex] = this.dataService.getDialogData();
        // And lastly refresh table
        this.refreshTable();
      }
    });
  }

  showType(i: number, _id: string, code: string, name: string, createdAt: string, policies: [], ) {
    this.index = i;
    
    const dialogRef = this.dialog.open(ShowDialogComponent, {
      data: { id: _id, code: code, name: name, policies: policies, created: createdAt}
    });


    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {

        this.refreshTable();
      }
    });
  }

  deleteType(i: number, _id: string, code: string, name: string) {
    this.index = i;
    // this.id = _id;
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      data: { id: _id, code: code, name: name }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        const foundIndex = this.typeData.dataChange.value.findIndex(x => x._id === this.id);
        // for delete we use splice in order to remove single object from DataService
        this.typeData.dataChange.value.splice(foundIndex, 1);
        this.refreshTable();
      }
    });
  }


  public refreshTable() {
    this.paginator._changePageSize(this.paginator.pageSize);
  }

  public loadData() {
    this.typeData = new PolicytypeService(this.httpClient);
    this.dataSource = new TypeDataSource(this.typeData, this.paginator, this.sort);
    fromEvent(this.filter.nativeElement, 'keyup')
      .pipe(debounceTime(150), distinctUntilChanged())
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }

        this.dataSource.filter = this.filter.nativeElement.value;
        // this.isLoadingResults = false
      });
  }
}


//Vehicle Data source

export class TypeDataSource extends DataSource<Policytype> {
  _filterChange = new BehaviorSubject('');

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: Policytype[] = [];

  renderedData: Policytype[] = [];


  constructor(public _typeService: PolicytypeService,
    public _paginator: MatPaginator,
    public _sort: MatSort) {
    super();
    // Reset to the first page when the user changes the filter.
    this._filterChange.subscribe(() => this._paginator.pageIndex = 0);
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<Policytype[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this._typeService.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];
    this._typeService.getAllTypes();


    return merge(...displayDataChanges).pipe(map(() => {
      this.filteredData = this._typeService.data.slice().filter((type: Policytype) => {
        const searchStr = (type.code + type.name).toLowerCase();
        return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
      });

      // Sort filtered data
      const sortedData = this.sortData(this.filteredData.slice());

      // Grab the page's slice of the filtered sorted data.
      const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
      this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
      return this.renderedData;
    }
    ));
  }

  disconnect() { }


  /** Returns a sorted copy of the database data. */
    sortData(data: Policytype[]): Policytype[] {

    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';

      switch (this._sort.active) {
        case 'code': [propertyA, propertyB] = [a.code, b.code]; break;
        case 'name': [propertyA, propertyB] = [a.name, b.name]; break;
        case 'createdAt': [propertyA, propertyB] = [a.createdAt, b.createdAt]; break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    });
  }
}

